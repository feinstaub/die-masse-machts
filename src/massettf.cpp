#include "massettf.h"

#include <iostream>
#include <sstream>
#include <algorithm>
#include <stdexcept>
#include "SDL_ttf.h"
#include "fileutil.h"

MasseTtf::MasseTtf()
{
}

void MasseTtf::Init(SDL_Renderer* sdlRenderer, const std::string &fontbasepath)
{
    if (TTF_Init() == -1)
    {
        std::stringstream ss;
        ss << "TTF_Init: " << TTF_GetError();
        throw std::runtime_error(ss.str());
    }

    _fontbasepath = fontbasepath;
    _sdlRenderer = sdlRenderer;
}

void MasseTtf::Quit()
{
    for_each(_fontList.begin(), _fontList.end(), TTF_CloseFont);
    TTF_Quit();
}

TTF_Font* MasseTtf::LoadFontFromRes(const std::string& relative_fontpath, int fontsize)
{
    TTF_Font* font = TTF_OpenFont(FileUtil::PathCombine(_fontbasepath, relative_fontpath).c_str(), fontsize);
    if (!font) {
        std::stringstream ss;
        ss << "TTF_OpenFont: " << TTF_GetError();
        throw std::runtime_error(ss.str());
    }

    _fontList.push_back(font); // save for later deletion
    return font;
}

SDL_Texture* MasseTtf::RenderToTexture(TTF_Font* font, const std::string &text)
{
    // Render some text in solid black to a new surface
    // then blit to the upper left of the screen
    // then free the text surface
    //SDL_Surface *screen;
    SDL_Color color = {0, 0, 0, 0};
    SDL_Surface *text_surface;
    if (!(text_surface = TTF_RenderText_Blended(font, text.c_str(), color))) {
        //handle error here, perhaps print TTF_GetError at least
        std::cerr << "MasseTtf::RenderToTexture: " << TTF_GetError() << std::endl;
        exit(5);
    } else {
        SDL_Texture* texture = SDL_CreateTextureFromSurface(_sdlRenderer, text_surface);
        //SDL_BlitSurface(text_surface,NULL,screen,NULL);
        //perhaps we can reuse it, but I assume not for simplicity.
        SDL_FreeSurface(text_surface);

        return texture;
    }
}
