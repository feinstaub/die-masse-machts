﻿#ifdef WIN32
#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#endif

#ifdef _MSC_VER
#pragma once
#endif

#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <stdexcept>
#include <string>
#include <sstream>
#include <unistd.h>
#include <SDL.h>
#include <SDL_image.h>
#include "fileutil.h"
#include "massettf.h"
#include "massetexturemgr.h"
#include "masseresources.h"
#include "CSDL_sound.h"
#include "massesdlutil.h"

#define VERSION "2.1"

/**
 * Globals:
 */
SDL_Window *_sdlWindow;
SDL_Renderer *_sdlRenderer;

MasseTtf _masseTtf;
MasseResources _masseRes(&_masseTtf);

int		_cursorX;
int		_cursorY;
bool	_isMouseDown = false;
bool    _isFullscreen = false;
bool	_isButtonClicked = false;
bool    _showHelp = false;

Uint32		_ticks;
Uint32		_duration;
int			_frameCounter;
const int	_nSpeed = 50;		// Millisekunden zw. den Frames der Animation

CSDL_sound	_sndMgr;			// for playing sounds
int			_playerSnd[2];	// 0: "Player 1", 1: "Next Player"
int			_pileupSnd;
int			_drawgameSnd;	// "Draw game! Do you want to play again?"
int			_playagainSnd;
int			_backgroundmusic;	// Backgroundmusic

/*
 * GAME_STATUS contains all status' (like a circle)
 */
typedef enum
{
    SPLASH_SCREEN = 0,
    PLAYER_ONE,
    PLAYER_ONE_PILEUP,
    PLAYER_TWO,
    PLAYER_TWO_PILEUP,
    DRAWGAME,
    YESNO,
    OUTRO
} GAME_STATUS;

GAME_STATUS	_gameStatus;

/*
 * forward declarations
 */
void resourcesEtcInit();
void gameInit();
void sdlDraw();
bool loadSounds();
void drawIntro();
void drawOutro();
void drawBackground();
void drawCaptionPlayer(int);
void drawPushButton(int, bool);
void drawPileAnim(int, bool);
void drawDrawGame();
void drawYesNo();
void drawCursor();
void drawIntroOutro(bool bOutro);
void drawHelp();

/*
 * main
 */
#ifdef WIN32 // Windows (including mingw)

void suppress_unused_warnings(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
{
    //char buffer[1000];
    //snprintf(buffer, 1000, "%d%d%s%d", (unsigned long)hInstance, (unsigned long)hPrevInstance, lpCmdLine, nCmdShow);

    std::stringstream ss;
    ss << hInstance << hPrevInstance << lpCmdLine << nCmdShow;
}

int WINAPI WinMain(
        HINSTANCE hInstance,  // handle to current instance
        HINSTANCE hPrevInstance,  // handle to previous instance
        LPSTR lpCmdLine,      // pointer to command line
        int nCmdShow          // show state of window
        )
{
    suppress_unused_warnings(hInstance, hPrevInstance, lpCmdLine, nCmdShow);

#else // Linux

int main(int argc, char **argv)
{

#endif

    //    // find working dir
    //    std::ofstream myfile;
    //    myfile.open ("___example.txt");
    //    myfile << "Writing this to a file.\n";
    //    myfile.close();

#ifdef QT_DECLARATIVE_DEBUG // if debug
    chdir(".."); // to one level up to where all the resources are
#endif

    try
    {
        /*
         * initialize SDL: video and sound
         */
        if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_AUDIO) < 0)
        {
            fprintf(stderr, "Unable to initialize SDL: %s\n", SDL_GetError());
            exit(1);
        }

        /*
         * call SDL_Quit and SDL_CloseAudio at exit => a clean exit
         */
        atexit(SDL_CloseAudio);
        atexit(SDL_Quit);

        _sdlWindow = SDL_CreateWindow("Die Masse macht's " VERSION " (Free the masses)",
                                       SDL_WINDOWPOS_UNDEFINED,
                                       SDL_WINDOWPOS_UNDEFINED,
                                       1024, 768, // TODO: use 0, 0
                                       SDL_WINDOW_RESIZABLE); // TODO: use SDL_WINDOW_FULLSCREEN_DESKTOP later

        if (_sdlWindow == NULL)
        {
            fprintf(stderr, "Unable to create OpenGL screen: %s\n", SDL_GetError());
            SDL_Quit();
            exit(2);
        }

        _sdlRenderer = SDL_CreateRenderer(_sdlWindow, -1, 0);

        SDL_SetHint(SDL_HINT_RENDER_SCALE_QUALITY, "linear");  // make the scaled rendering look smoother.
        SDL_RenderSetLogicalSize(_sdlRenderer, 1024, 768); // keep aspect ratio

        if (_sdlRenderer == NULL)
        {
            std::cerr << "Unable to create OpenGL screen: " << SDL_GetError() << std::endl;
            SDL_Quit();
            exit(2);
        }

        SDL_SetRelativeMouseMode(SDL_TRUE);

        resourcesEtcInit();
        gameInit();

        bool bDone = false;

        while (!bDone)
        {
            SDL_Event event;
            while (SDL_PollEvent(&event))
            {
                switch (event.type)
                {
                case SDL_QUIT:
                    bDone = true;
                    break;

                case SDL_KEYDOWN:
                    if (event.key.keysym.sym == SDLK_ESCAPE)
                    {
                        _gameStatus = OUTRO;
                    }
                    else if (event.key.keysym.sym == SDLK_h)
                    {
                        _showHelp = true;
                    }
                    else if (event.key.keysym.sym == SDLK_f)
                    {
                        if (_isFullscreen)
                        {
                            if (0 == SDL_SetWindowFullscreen(_sdlWindow, 0))
                            {
                                _isFullscreen = !_isFullscreen;
                            }
                            else
                            {
                                std::cout << "SDL_SetWindowFullscreen failed" << std::endl;
                            }
                        }
                        else
                        {
                            if (0 == SDL_SetWindowFullscreen(_sdlWindow, SDL_WINDOW_FULLSCREEN_DESKTOP))
                            {
                                _isFullscreen = !_isFullscreen;
                            }
                            else
                            {
                                std::cout << "SDL_SetWindowFullscreen failed" << std::endl;
                            }
                        }
                    }
//                    // DEBUG:
//                    else if (event.key.keysym.sym == SDLK_v)
//                    {
//                        SDL_Rect vportrect = { 0, 0, 800, 300 };
//                        SDL_RenderSetViewport(_sdlRenderer, &vportrect);

//                    }
                    break;

                case SDL_KEYUP:
                    if (event.key.keysym.sym == SDLK_h)
                    {
                        _showHelp = false;
                    }
                    break;

                case SDL_MOUSEMOTION:
                    {
//                        // DEBUG:
//                        // keep cursor in window begin
//                        // TODO: fullscreen handling
//                        int w;
//                        int h;
//                        SDL_GetWindowSize(_sdlWindow, &w, &h);

//                        // DEBUG:
//                        SDL_Rect vportrect;
//                        SDL_RenderGetViewport(_sdlRenderer, &vportrect);

//                        double f = 2.0; // .x and .y is always the half. Why?
//                        int motionx = event.motion.x * f;
//                        int motiony = event.motion.y * f;

//                        // DEBUG:
//                        cout << "vportrect:" << vportrect.x << "," << vportrect.y << ","
//                                << vportrect.w << "," << vportrect.h << ". "
//                             << "w" << w << " h" << h << " moX" << motionx << " moY" << motiony << endl;
//                        }
//                        // keep cursor in window end

                        double speed_factor = 1.5;
                        _cursorX += event.motion.xrel * speed_factor; // = motionx;
                        _cursorY += event.motion.yrel * speed_factor; // = motiony;
                        if (_cursorX < 0) _cursorX = 0;
                        if (_cursorX > 1024) _cursorX = 1024;
                        if (_cursorY < 0) _cursorY = 0;
                        if (_cursorY > 768) _cursorY = 768;
                    }
                    break;

                case SDL_MOUSEBUTTONDOWN:
                {
                    _isMouseDown = true;
                    //cout << "g_bMouseDown=" << _isMouseDown << endl;

                    GAME_STATUS oldStatus = _gameStatus;

                    switch (_gameStatus)
                    {
                    case SPLASH_SCREEN:
                        _gameStatus = PLAYER_ONE;
                        _sndMgr.PlaySDLSound(_playerSnd[0]);
                        break;

                    case PLAYER_ONE:
                        break;

                    case PLAYER_ONE_PILEUP:
                        break;

                    case PLAYER_TWO:
                        _duration = 0;
                        break;

                    case PLAYER_TWO_PILEUP:
                        break;

                    case DRAWGAME:
                        break;

                    case YESNO:
                        break;

                    case OUTRO:
                        bDone = true;
                        break;
                    }

                    if (oldStatus != _gameStatus)
                        _isButtonClicked = false;
                }
                    break;

                case SDL_MOUSEBUTTONUP:
                    _isMouseDown = false;
                    //cout << "g_bMouseDown=" << _isMouseDown << endl;
                    break;
                }
            }

            SDL_RenderClear(_sdlRenderer);
            sdlDraw();
            SDL_RenderPresent(_sdlRenderer);
        }

        _masseRes.FreeAll();
        _masseTtf.Quit();
    }
    catch (std::exception &e)
    {
        std::cerr << "Exception: " << e.what() << std::endl;
        return 1;
    }

    return 0;
}

/**
 *
 */
void resourcesEtcInit()
{
    // init ttf before _masseRes.LoadAll();
    _masseTtf.Init(_sdlRenderer, FileUtil::PathCombine(_masseRes._res_basepath, "font"));

    _masseRes.SetSdlRenderer(_sdlRenderer);
    _masseRes.LoadAll();

    /*
     * load sounds (error exits)
     */
    if (!loadSounds())
    {
        fprintf(stderr, "Unable to load sound.");
        exit(4);
    }

    /*
     * Start playing sounds and start backgroundmusic
     */
    _sndMgr.StartPlaying();
    _sndMgr.PlaySDLSound(_backgroundmusic);
    _sndMgr.Loop(_backgroundmusic, true);

    SDL_ShowCursor(SDL_DISABLE);
}

bool loadSounds()
{
    std::string basepath = FileUtil::PathCombine(_masseRes._res_basepath, "audio/");

    _playerSnd[0]	= _sndMgr.LoadWav(basepath + "player1.wav");	// 0: "Player 1", 1: "Next Player"
    _playerSnd[1]	= _sndMgr.LoadWav(basepath + "player2.wav");

    _pileupSnd		= _sndMgr.LoadWav(basepath + "pileup.wav");
    _drawgameSnd	= _sndMgr.LoadWav(basepath + "drawgame.wav");	// "Draw game"
    _playagainSnd	= _sndMgr.LoadWav(basepath + "playagain.wav");	//! Do you want to play again?

    _backgroundmusic=_sndMgr.LoadWav(basepath + "background.wav");

    if (_drawgameSnd	== -1 ||
            _playerSnd[0]	== -1 ||
            _playerSnd[1]	== -1 ||
            _pileupSnd		== -1 ||
            _playagainSnd	== -1 ||
            _backgroundmusic == -1)
    {
        return false;
    }
    else
    {
        return true;
    }
}

void gameInit()
{
    _gameStatus = SPLASH_SCREEN;
}

void sdlDraw()
{
    switch (_gameStatus)
    {
    case SPLASH_SCREEN:
        drawIntro();
        break;

    case PLAYER_ONE:
        drawBackground();
        drawCaptionPlayer(0);
        drawPushButton(0, _isMouseDown);
        break;

    case PLAYER_ONE_PILEUP:
        drawBackground();
        drawCaptionPlayer(0);
        drawPushButton(0, _isMouseDown);
        drawPileAnim(0, true);
        break;

    case PLAYER_TWO:
        drawBackground();
        drawCaptionPlayer(0);
        drawCaptionPlayer(1);
        drawPushButton(1, _isMouseDown);
        drawPileAnim(0, false);
        break;

    case PLAYER_TWO_PILEUP:
        drawBackground();
        drawCaptionPlayer(0);
        drawCaptionPlayer(1);
        drawPushButton(1, _isMouseDown);
        drawPileAnim(0, false);
        drawPileAnim(1, true);
        break;

    case DRAWGAME:
    {
        drawBackground();
        drawCaptionPlayer(0);
        drawCaptionPlayer(1);
        drawPileAnim(0, false);
        drawPileAnim(1, false);
        drawDrawGame();

        if (SDL_GetTicks() - _duration > 800)
        {
            _sndMgr.PlaySDLSound(_playagainSnd);
            _gameStatus = YESNO;
            _isButtonClicked = false;
        }
    }

        break;

    case YESNO:
        drawBackground();
        drawCaptionPlayer(0);
        drawCaptionPlayer(1);
        drawPileAnim(0, false);
        drawPileAnim(1, false);
        drawDrawGame();
        drawYesNo();
        break;

    case OUTRO:
        drawOutro();
    }

    drawCursor();
}

void drawIntro()
{
    drawIntroOutro(false);
}

void drawOutro()
{
    drawIntroOutro(true);
}

void drawIntroOutro(bool bOutro)
{
    SDL_RenderCopy(_sdlRenderer, _masseRes._imgSplash, NULL, NULL);

    if (bOutro)
    {
        SDL_Rect rect = {400, 350, 450, 88 }; // TODO: scale up this image!
        SDL_RenderCopy(_sdlRenderer, _masseRes._imgClickToQuit, NULL, &rect);
    }
    else
    {
        MasseSdlUtil::RenderCopyWithTextureSize(_sdlRenderer, _masseRes._texTextBasedOnSDL2, 100, 300);
        MasseSdlUtil::RenderCopyWithTextureSize(_sdlRenderer, _masseRes._texTextGppEdition, 750, 300);
        if (!_showHelp)
        {
            MasseSdlUtil::RenderCopyWithTextureSize(_sdlRenderer, _masseRes._texPressHForHelp, 105, 625);
        }
        else
        {
            drawHelp();
        }
    }
}

void drawHelp()
{
    MasseSdlUtil::RenderCopyWithTextureSize(_sdlRenderer, _masseRes._texHelpLine1, 250, 400);
    MasseSdlUtil::RenderCopyWithTextureSize(_sdlRenderer, _masseRes._texHelpLine2, 250, 450);
    MasseSdlUtil::RenderCopyWithTextureSize(_sdlRenderer, _masseRes._texHelpLine3, 250, 500);
}

void drawBackground()
{
    SDL_RenderCopy(_sdlRenderer, _masseRes._imgBack, NULL, NULL);
}

void drawCaptionPlayer(int nPlayer)
{
    SDL_Rect rect;

    rect.w = 450;
    rect.h = 88;

    if (nPlayer == 0)
    {
        rect.x = 0;
        rect.y = 0;
    }
    else
    {
        rect.x = 574;
        rect.y = 0;
    }

    SDL_RenderCopy(_sdlRenderer, _masseRes._imgPlayer[nPlayer], NULL, &rect);
}


/**
 * nKind: 0: Player 1 anhäufen
 *        1: Player 2 anhäufen
 *        2: No
 *        3: yes
 */
void drawPushButton(int nKind, bool bPressed)
{
    SDL_Rect rect = { 0, 0, 300, 59 };

    switch (nKind)
    {
    case 0:
        rect.x = 50;
        rect.y = 110;
        break;

    case 1:
        rect.x = 674;
        rect.y = 110;
        break;

    case 2:
        rect.x = 200;
        rect.y = 410;
        break;

    case 3:
        rect.x = 524;
        rect.y = 410;
        break;
    }

    if (bPressed && !_isButtonClicked)
    {
        if (_cursorX > rect.x && _cursorX < rect.x + rect.w
                && _cursorY > rect.y && _cursorY < rect.y + rect.h)
        {
            switch (nKind)
            {
            case 0:
                _gameStatus = PLAYER_ONE_PILEUP;
                _frameCounter = 0;
                _ticks = SDL_GetTicks();
                _sndMgr.PlaySDLSound(_pileupSnd);
                _isButtonClicked = true;
                break;

            case 1:
                _gameStatus = PLAYER_TWO_PILEUP;
                _frameCounter = 0;
                _ticks = SDL_GetTicks();
                _sndMgr.PlaySDLSound(_pileupSnd);
                _isButtonClicked = true;
                break;

            case 2:
                _gameStatus = OUTRO;
                break;

            case 3:
                _isButtonClicked = false;
                _gameStatus = PLAYER_ONE;
                _sndMgr.PlaySDLSound(_playerSnd[0]);
                break;
            }
        }
        else
        {
            bPressed = false;
        }
    }

    if (bPressed)
    {
        rect.x += 5;	// damit der Button runtergeht
        rect.y += 5;
    }

    SDL_RenderCopy(_sdlRenderer, _masseRes._imgButton[bPressed], NULL, &rect);

    switch (nKind)
    {
    case 0:
    case 1:
        SDL_RenderCopy(_sdlRenderer, _masseRes._imgPileupText, NULL, &rect);
        break;

    case 2:
        SDL_RenderCopy(_sdlRenderer, _masseRes._imgNoText, NULL, &rect);
        break;

    case 3:
        SDL_RenderCopy(_sdlRenderer, _masseRes._imgYesText, NULL, &rect);
        break;
    }
}

void drawCursor()
{
    SDL_Rect rect = { _cursorX, _cursorY, 64, 64 };
    SDL_RenderCopy(_sdlRenderer, _masseRes._imgCursor, NULL, &rect);
}

void drawDrawGame()
{
    SDL_Rect rect = { 250, 130, 500, 300 };
    SDL_RenderCopy(_sdlRenderer, _masseRes._imgDrawGame, NULL, &rect);
}

void drawYesNo()
{
    drawPushButton(2, _isMouseDown);
    drawPushButton(3, _isMouseDown);
}

/**
 * bAnimate == false: Nur der letzte Frame der Animation wird angezeigt
 */
void drawPileAnim(int nPlayer, bool bAnimate)
{
    SDL_Rect rect = { 0, 0, 300, 500 };

    if (nPlayer == 0)
    {
        rect.x = 50;
        rect.y = 200;
    }
    else
    {
        rect.x = 674;
        rect.y = 200;
    }

    if (bAnimate)
    {
        SDL_RenderCopy(_sdlRenderer, _masseRes._pileAnim[_frameCounter], NULL, &rect);
        if (SDL_GetTicks() - _ticks > _nSpeed)
        {
            _ticks = SDL_GetTicks();
            _frameCounter += 2;
            if (_frameCounter >= _masseRes._nPileAnimNum - 1)
            {
                if (nPlayer == 0)
                {
                    _isButtonClicked = false;
                    _gameStatus = PLAYER_TWO;
                    _sndMgr.PlaySDLSound(_playerSnd[1]);
                }
                else
                {
                    _sndMgr.PlaySDLSound(_drawgameSnd);
                    _gameStatus = DRAWGAME;
                    _duration = SDL_GetTicks();
                }
            }
        }
    }
    else
    {
        SDL_RenderCopy(_sdlRenderer, _masseRes._pileAnim[_masseRes._nPileAnimNum - 1], NULL, &rect);
    }
}
