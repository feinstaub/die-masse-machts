#include "massetexturemgr.h"

MasseTextureMgr::MasseTextureMgr()
{
}

SDL_Texture* MasseTextureMgr::RegisterTexture(SDL_Texture* texture)
{
    _textureList.push_back(texture);
    return texture;
}

void MasseTextureMgr::DestroyAll()
{
    // NOTE: caused segfault when thousands of font textures were deleted
    // 2019: still valid?

    for (auto& texture : _textureList)
    {
        SDL_DestroyTexture(texture);
    }
}
