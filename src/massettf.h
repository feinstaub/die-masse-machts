#ifndef MASSETTF_H
#define MASSETTF_H

#include <string>
#include <vector>
#include "SDL_ttf.h"

class MasseTtf
{
public:
    MasseTtf();

    /**
     * @brief Calls TTF_Init
     * @param sdlRenderer
     * @param fontbasepath
     */
    void Init(SDL_Renderer* sdlRenderer, const std::string& fontbasepath);

    /**
     * @brief frees resources and calls TTF_Quit
     */
    void Quit();

    /**
     * @brief Loads a font
     * @param fontbasepath
     * @param fontsize
     * @return The returned font will be cleaned automatically with a call to Quit()
     */
    TTF_Font* LoadFontFromRes(const std::string& relative_fontpath, int fontsize);

    /**
     * @brief RenderToTexture
     * @param text
     * @return texture, must be freed
     */
    SDL_Texture* RenderToTexture(TTF_Font* font, const std::string& text);

private:
    SDL_Renderer* _sdlRenderer;
    std::string _fontbasepath;
    std::vector<TTF_Font*> _fontList;
};

#endif // MASSETTF_H
