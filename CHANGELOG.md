die-masse-machts changes
========================

2.1
---
* Build with CMake
* Drop old Win32 support (might be added again in the future)
* At the end of one cycle background.wav plays noise instead of playing from the beginning
* Press H for help

2.0
---
* 425165e58d6eb7e753c368b45d430d11bbd1fcd1
    * last publish_master 2013
* 5f31060b69e0ee3b99e9b23e6c65265053fc1c40
    * Fix segfault at exit
* 5a410a459526e35e295940c6bc27067151be5e4c - 2013
    * port_to_2.0
        -    g_sdlWindow = SDL_CreateWindow("Die Masse macht's (Free trial)",
        +    g_sdlWindow = SDL_CreateWindow("Die Masse macht's 2.0 (Free trial)",
* a4179ce31bf624473b1e528252d5e025cd4d01ed - 2013
    * port_to_2.0

1.1
---
* faa35d75786ef090b4b7f0711f232f3dbbd97534
    * tag v1.1
