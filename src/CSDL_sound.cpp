// CSDL_sound.cpp: implementation of the CSDL_sound class.
//
//////////////////////////////////////////////////////////////////////

#include "CSDL_sound.h"

#include <cstdio>
#include <iostream>
#include <string.h> // memset

CSDL_sound::CSDL_sound()
{
    /*
     * no audio-files are loaded
     */
    n_loaded_sounds = 0;

    /*
     * initializing AudioSpec etc...
     */
    InitSDL_Sound();
}

CSDL_sound::~CSDL_sound()
{
    /*
     * to delete all allocated samples audioplaying has to be stopped
     */
    SDL_PauseAudio(1);

    /*
     * delete all samples... (really important - otherwise: memory leaks)
     */
    std::vector<sound_t>::iterator it;

    for (it = all_sounds.begin(); it != all_sounds.end(); it++)
    {
        SDL_free(it->samples);
    }

    /*
     * free vectors
     */
    playing		.clear();
    all_sounds	.clear();
}

void AudioCallback(void *user_data, Uint8 *audio, int length)
{
    CSDL_sound* pSoundClass = (CSDL_sound*)user_data;

    /*
     * Clear the audio buffer so we can mix samples into it.
     */
    memset(audio, 0, length);

    /*
     * Mix in each sound.
     */
    std::vector<playing_t>::iterator it;

    for (it = pSoundClass->access_playing().begin();
         it != pSoundClass->access_playing().end();
         it++)
    {
        if (it->active)
        {

            /* Locate this sound's current buffer position. */
            Uint8 *sound_buf = it->sound->samples;
            sound_buf += it->position;

            /* Determine the number of samples to mix. */
            Uint32 sound_len = it->sound->length - it->position;
            if (sound_len > length)
            {
                sound_len = length;
            }

            /* Mix this sound into the stream. */
            SDL_MixAudio(audio, sound_buf, sound_len, SDL_MIX_MAXVOLUME / 2);

            /* Update the sound buffer's position. */
            it->position += length;

            /* Have we reached the end of the sound? */
            if (it->position >= it->sound->length)
            {
                if (it->sound->loop) /* if you want to loop */
                {
                    it->position = 0;
                }
                else
                {
                    it->active = 0;	/* mark it inactive */
                }
            }
        }
    }
}

bool CSDL_sound::InitSDL_Sound()
{
    /* Open the audio device. The sound driver will try to give us
     * the requested format, but it might not succeed. The 'obtained'
     * structure will be filled in with the actual format data.
     */
    desired.freq = 44100;		/* desired output sample rate */
    desired.format = AUDIO_S16;	/* request signed 16-bit samples */
    desired.samples = 4096;		/* this is more or less discretionary */ //4096
    desired.channels = 2;		/* ask for stereo */
    desired.callback = AudioCallback;
    desired.userdata = this;

    if (SDL_OpenAudio(&desired, &obtained) < 0)
    {
        printf("Unable to open audio device: %s\n", SDL_GetError());
        return false;
    }

    SDL_PauseAudio(1);

    return true;
}


bool CSDL_sound::LoadAndConvertSound(const char *filename, SDL_AudioSpec *spec, sound_p sound)
{
    SDL_AudioSpec loaded;       /* format of the loaded data */
    Uint8 *audio_buf;
    Uint32 audio_len;
    /* Load the WAV file in its original sample format. */
    if (SDL_LoadWAV(filename, &loaded, &audio_buf, &audio_len) == NULL)
    {
        printf("Unable to load sound: %s\n", SDL_GetError());
        return false;
    }

    /* Build a conversion structure for converting the samples.
     * This structure contains the data SDL needs to quickly
     * convert between sample formats.
     * https://wiki.libsdl.org/SDL_BuildAudioCVT
     */
    SDL_AudioCVT cvt;           /* audio format conversion structure */
    if (SDL_BuildAudioCVT(&cvt, loaded.format,
              loaded.channels,
              loaded.freq,
              spec->format, spec->channels, spec->freq) < 0) {
        printf("Unable to convert sound: %s\n", SDL_GetError());
        return false;
    }

    //std::cout << "A: " << loaded.format << " " << (int)(loaded.channels) << " " << loaded.freq << std::endl;
    //std::cout << "B: " << spec->format  << " " << (int)(spec->channels)  << " " << spec->freq << std::endl;

    /* Since converting PCM samples can result in more data
     * (for instance, converting 8-bit mono to 16-bit stereo),
     * we need to allocate a new buffer for the converted data.
     * Fortunately SDL_BuildAudioCVT supplied the necessary
     * information.
     */
    cvt.len = audio_len;
    Uint8 *new_buf;
    new_buf = (Uint8 *)SDL_malloc(cvt.len * cvt.len_mult);

    if (new_buf == NULL)
    {
        printf("Memory allocation failed.\n");
        SDL_FreeWAV(audio_buf);
        return false;
    }

    /* Copy the sound samples into the new buffer. */
    memcpy(new_buf, audio_buf, audio_len);
    cvt.buf = new_buf;

    /* Perform the conversion on the new buffer. */
    if (SDL_ConvertAudio(&cvt) < 0)
    {
        printf("Audio conversion error: %s\n", SDL_GetError());
        SDL_free(new_buf);
        SDL_FreeWAV(audio_buf);
        return false;
    }

    /* Swap the converted data for the original. */
    SDL_FreeWAV(audio_buf);
    sound->samples = cvt.buf;
    sound->length = cvt.len * cvt.len_ratio;
    // (NOT "* cvt.len_mult" which is the internal buffer which might be bigger than the final length).
    // In the case of 22050 mono to 44100 stereo conversion cvt.len_mult is 16 but cvt.len_ratio is 4.
    // If the former is used then we get noise after the original length of the wave file

    /* Success! */
    printf("Sound file loaded: '%s'\n", filename);

    return true;
}

int CSDL_sound::LoadWav(std::string filename)
{
    sound_t newSound;

    if (LoadAndConvertSound(filename.c_str(), &obtained, &newSound))
    {
        n_loaded_sounds++;
        newSound.nID = n_loaded_sounds;
        newSound.loop = false;
        all_sounds.push_back(newSound);
        return n_loaded_sounds;
    }

    return -1;
}

void CSDL_sound::StopAllSounds()
{
    playing.clear();
}

/*
 * plays the sound with the sound-id sndID
 */
bool CSDL_sound::PlaySDLSound(int sndID)
{
    sound_p sound;
    bool existent = false;

    std::vector<sound_t>::iterator it;

    for (it = all_sounds.begin(); it != all_sounds.end(); it++)
    {
        if (it->nID == sndID)
        {
            sound = &(*it);
            existent = true;
        }
    }

    if (!existent)
    {
        return false;
    }

    playing_t newPlaying;

    /*
     * The 'playing' structures are accessed by the audio callback,
     * so we should obtain a lock before we access them.
     */
    SDL_LockAudio();
    newPlaying.active = 1;
    newPlaying.sound = sound;
    newPlaying.position = 0;
    SDL_UnlockAudio();

    playing.push_back(newPlaying);

/*    SDL_LockAudio();
    playing.end()->active = 1;
    playing.end()->sound = sound;
    playing.end()->position = 0;
    SDL_UnlockAudio();
*/
    return true;
}

/*
 * sets the sound with sndID to true / false
 * return: [true ] on success
 *         [false] on failure: sndID not found!
 */
bool CSDL_sound::Loop(int sndID, bool loop)
{
    bool existent = false;
    std::vector<sound_t>::iterator it;

    for (it = all_sounds.begin(); it != all_sounds.end(); it++)
    {
        if (it->nID == sndID)
        {
            it->loop = loop;
            existent = true;
        }
    }

    if (!existent)
    {
        return false;
    }
    else
    {
        return true;
    }
}

void CSDL_sound::PausePlaying()
{
    SDL_PauseAudio(1);
}

void CSDL_sound::StartPlaying()
{
    SDL_PauseAudio(0);
}
