#include "fileutil.h"
#include "masseresources.h"
#include <string>
#include <sstream>
#include <iostream>
#include <stdexcept>
#include "SDL_image.h"

MasseResources::MasseResources(MasseTtf *masseTtf)
{
    _res_basepath = "app_resources";
    _masseTtf = masseTtf;
}

void MasseResources::LoadAll()
{
    load_textures();
}

void MasseResources::FreeAll()
{
    _masseTextureMgr.DestroyAll();
}

/*
 * loads all bitmaps / throws on error
 * return [true ] if all bitmaps were loaded correctrly
 *        [false] if an error occured (=file not found)
 */
void MasseResources::load_textures()
{
    // load fonts (before textures)
    _fontFreemetto48 = _masseTtf->LoadFontFromRes("freemetto/freemetto.ttf", 48);
    _fontGrandhotel48 = _masseTtf->LoadFontFromRes("grandhotel/GrandHotel-Regular.otf", 48);
    _fontGrandhotel24 = _masseTtf->LoadFontFromRes("grandhotel/GrandHotel-Regular.otf", 24);

    // load textures
    _imgBack = load_image_from_file("img/back.jpg");
    _imgSplash = load_image_from_file("img/splash.jpg");
    _imgCursor = load_image_from_file("img/cursor.gif");
    _imgPlayer[0] = load_image_from_file("img/player1.gif");
    _imgPlayer[1] = load_image_from_file("img/player2.gif");

    _imgButton[0] = load_image_from_file("img/button_up.gif");
    _imgButton[1] = load_image_from_file("img/button_down.gif");
    SDL_SetTextureAlphaMod(_imgButton[0], 175);
    SDL_SetTextureAlphaMod(_imgButton[1], 175);

    _imgPileupText = load_image_from_file("img/pileup.gif");
    _imgNoText = load_image_from_file("img/no.gif");
    _imgYesText = load_image_from_file("img/yes.gif");
    _imgDrawGame = load_image_from_file("img/drawgame.gif");
    _imgClickToQuit = load_image_from_file("img/quittext.gif");

    for (int i = 0; i < _nPileAnimNum; ++i)
    {
        std::string path = "anim/pile/h";
        char numBuf[3];
        snprintf(numBuf, 3, "%02d", i + 1);
        std::string combined = path + numBuf + ".gif";
        _pileAnim[i] = load_image_from_file(combined);
    }

    _texTextBasedOnSDL2 = _masseTextureMgr.RegisterTexture(_masseTtf->RenderToTexture(
        _fontFreemetto48, "SDL2"));

    _texTextGppEdition = _masseTextureMgr.RegisterTexture(_masseTtf->RenderToTexture(
        _fontGrandhotel48, "g++ Edition"));

    _texPressHForHelp = _masseTextureMgr.RegisterTexture(_masseTtf->RenderToTexture(
        _fontGrandhotel24, "Press H for Help"));

    _texHelpLine1 = _masseTextureMgr.RegisterTexture(_masseTtf->RenderToTexture(
        _fontFreemetto48, "F - Toggle Fullscreen"));
    _texHelpLine2 = _masseTextureMgr.RegisterTexture(_masseTtf->RenderToTexture(
        _fontFreemetto48, "Click to begin"));
    _texHelpLine3 = _masseTextureMgr.RegisterTexture(_masseTtf->RenderToTexture(
        _fontFreemetto48, "ESC - Exit"));
}

/**
 * @brief Load image from file, convert to surface,
 *  convert to texture and register texture for later deletion.
 *  Throws runtime_error on failure.
 * @param filepath
 * @return must be freed using SDL_DestroyTexture
 */
SDL_Texture* MasseResources::load_image_from_file(const std::string& filepath)
{
    SDL_Surface *tmpSurface = IMG_Load(FileUtil::PathCombine(_res_basepath, filepath).c_str());
    if (!tmpSurface)
    {
        std::stringstream ss;
        ss << "load_image_from_file: IMG_Load failed: " << SDL_GetError() << std::endl;
        throw std::runtime_error(ss.str());
    }
    SDL_Surface *surface = SDL_ConvertSurfaceFormat(tmpSurface, SDL_PIXELFORMAT_RGB888, 0); // TODO: which pixelformat is ok?
    SDL_FreeSurface(tmpSurface);

    SDL_Texture* texture = SDL_CreateTextureFromSurface(_sdlRenderer, surface);
    SDL_FreeSurface(surface);

    _masseTextureMgr.RegisterTexture(texture);

    return texture;
}

void MasseResources::SetSdlRenderer(SDL_Renderer *sdlRenderer)
{
    _sdlRenderer = sdlRenderer;
}

