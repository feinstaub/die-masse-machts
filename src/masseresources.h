#ifndef MASSERESOURCES_H
#define MASSERESOURCES_H

#include "SDL.h"
#include "massetexturemgr.h"
#include "massettf.h"
#include <string>

class MasseResources
{
public:
    MasseResources(MasseTtf* masseTtf);

    /**
     * @brief throws on error
     */
    void LoadAll();

    void FreeAll();

public:
    std::string _res_basepath;

    static const int _nPileAnimNum = 15;

    SDL_Texture* _pileAnim[_nPileAnimNum];
    SDL_Texture* _imgCursor;
    SDL_Texture* _imgBack;
    SDL_Texture* _imgSplash;
    SDL_Texture* _imgPileupText;
    SDL_Texture* _imgNoText;
    SDL_Texture* _imgYesText;
    SDL_Texture* _imgButton[2];	// 0: up; 1: down
    SDL_Texture* _imgPlayer[2];
    SDL_Texture* _imgDrawGame;
    SDL_Texture* _imgClickToQuit;
    SDL_Texture* _texTextBasedOnSDL2;
    SDL_Texture* _texTextGppEdition;
    SDL_Texture* _texPressHForHelp;
    SDL_Texture* _texHelpLine1;
    SDL_Texture* _texHelpLine2;
    SDL_Texture* _texHelpLine3;

    TTF_Font* _fontFreemetto48;
    TTF_Font* _fontGrandhotel48;
    TTF_Font* _fontGrandhotel24;

    void SetSdlRenderer(SDL_Renderer *sdlRenderer);

private:
    void load_textures();
    SDL_Texture* load_image_from_file(const std::string&);

private:
    SDL_Renderer *_sdlRenderer;
    MasseTtf* _masseTtf;

    MasseTextureMgr _masseTextureMgr;
};

#endif // MASSERESOURCES_H
