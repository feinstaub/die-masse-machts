#ifndef FILEUTIL_H
#define FILEUTIL_H

#include <string>

class FileUtil
{
public:
    FileUtil();

    static std::string PathCombine(const std::string& path1, const std::string& path2);
};

#endif // FILEUTIL_H
