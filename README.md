Die Masse macht's
=================
The popular 2-player game to pile up masses.

![Screenshot](/README_res/screenshot1.jpg "Screenshot 1")

Original teaser text
--------------------
*German*

Das Spiel "Die Masse macht's" ist für zwei Spieler und es geht darum,
den größten Berg Masse anzuhäufen! Die Spieler sehen eine "Masse-Anhäufungs-Animation",
die maximalen Spielspaß garantiert, es gibt einen Vollbildmodus,
sowie Hintergrundmusik und Sprechsounds (die man auch verändern kann).

*English*

The game "Die Masse macht's" is a two player game and is about to pile up
the biggest mountain of mass! The players can see a "piling up the mass animation"
that guarantees maximum gaming fun. There is a full screen mode, background music
as well as speaking sounds (that can be altered by the advanced user).

Building
--------
With CMake

    mkdir build
    cd build
    # for local tests we use a usr directory relative to the build dir
    cmake -Gninja -DCMAKE_INSTALL_PREFIX=usr ..
    ninja

Licenses
--------
* Program: GPLv3
* Fonts from http://openfontlibrary.org

Up to version 2.0
-----------------
### Code
Based on SDL.

Up to version 2.0: Compilable on Windows and Linux

