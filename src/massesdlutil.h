#ifndef MASSESDLUTIL_H
#define MASSESDLUTIL_H

#include "SDL.h"

class MasseSdlUtil
{
public:
    MasseSdlUtil();

public:
    static void RenderCopyWithTextureSize(SDL_Renderer *renderer, SDL_Texture *texture, int x, int y)
    {
        int w;
        int h;
        SDL_QueryTexture(texture, NULL, NULL, &w, &h);
        SDL_Rect rect = { x, y, w, h };
        SDL_RenderCopy(renderer, texture, NULL, &rect);
    }
};

#endif // MASSESDLUTIL_H
