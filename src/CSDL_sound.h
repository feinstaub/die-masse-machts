/***************************************************************************
 *                                                                         *
 *   This program(class) is free software; you can redistribute it and/or  *
 *   modify it under the terms of the GNU General Public License as        *
 *   published by the Free Software Foundation; either version 2 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

// CSDL_sound.h: interface for the CSDL_sound class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_CSDL_SOUND_H__114B1500_DBBF_46C6_B710_CC517743ACF1__INCLUDED_)
#define AFX_CSDL_SOUND_H__114B1500_DBBF_46C6_B710_CC517743ACF1__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <SDL.h>
#include <vector>		/* using stl (vector) for arrays */
#include <string>

/*
 * Structure for loaded sounds.
 */
typedef struct sound_s
{
    Uint8	*samples;	/* raw PCM sample data			*/
    Uint32	length;		/* size of sound data in bytes	*/
	int		nID;		/* ID of sound data				*/
	bool	loop;		/* true -> loops sound			*/
} sound_t, *sound_p;

/*
 * Structure for a currently playing sound.
 */
typedef struct playing_s
{
    int active;			/* 1 if this sound should be played */
    sound_p sound;		/* sound data to play */
    Uint32 position;	/* current position in the sound buffer */
} playing_t, *playing_p;

/*
 * class CSDL_sound. At the bottom of this file you have a small tutorial
 */
class CSDL_sound
{
private: // previously static
    /*
     * playing contains all currently played sounds
     * all_sounds contains all loaded sounds
     */
    std::vector<playing_t> playing;
    std::vector<sound_t> all_sounds;

public:
    std::vector<playing_t>& access_playing() { return playing; }

public:
	void	StartPlaying();					/* calls SDL_PauseAudio(0);	stops all sounds on their current position */
	void	PausePlaying();					/* calls SDL_PauseAudio(1); StartPlaying to continue */
	bool	Loop( int idSound, bool loop );	/* sets the sound with sndID to true / false	*/
	bool	PlaySDLSound( int sndID );		/* plays the sound with sndID					*/
	void	StopAllSounds();				/* cancels all sounds -> all sound-positions are set to 0; (does NOT call SDL_PauseAudio(1);) */
    int		LoadWav(std::string filename);		/* loads a wav-file and returns the id (sndID)	*/
	CSDL_sound();							/* Constructor									*/
	virtual ~CSDL_sound();					/* Destructor									*/

/*
 * private: the "u s e r" does not have any direct contact with this
 */
private:
	int n_loaded_sounds;					/* is used for new ids	*/
	SDL_AudioSpec obtained;					/* AudioSpec			*/
	SDL_AudioSpec desired;					/* AudioSpec			*/
	/** is called by LoadWav
     * If -1 is returned, there was an error but all memory was freed; so nothing to do then
     */
    bool LoadAndConvertSound(const char *filename, SDL_AudioSpec *spec, sound_p sound);
	bool InitSDL_Sound();					/* is called by the constructor and initializes the AudioSpecs	*/
};

#endif // !defined(AFX_CSDL_SOUND_H__114B1500_DBBF_46C6_B710_CC517743ACF1__INCLUDED_)

/* TUTORIAL: EXAMPLE ( main.cpp )
 *
 *	#include "csdl_sound.h"
 *
 *	int main()
 *	{
 *		SDL_Init(SDL_INIT_VIDEO | SDL_INIT_AUDIO); // important: SDL_INIT_AUDIO
 *		atexit(SDL_Quit		 );
 *		atexit(SDL_CloseAudio); // really important to close audio on exit, too
 *
 *		CSDL_sound snd;			// sound object
 *		int snd1;
 *		int snd2;
 *
 *		snd1 = snd.LoadWav("explosion.wav"); // returns ID of the sound data
 *		snd2 = snd.LoadWav("bgmusic.wav"  ); // returns ID of the sound data
 *
 *		if (snd1 == .1 || snd2 == -1)
 *		{
 *			printf("Could not load soundfile(s)");
 *			exit(1);	// you do not have to exit
 *		}
 *
 *		snd.Loop(snd2, true);	// loop snd2 (like backgroundmusic)
 *								// return false, if id of snd2 was not found
 *		snd.PlaySDLSound(snd2);	// start playing background-music
 *
 *		while ( 1 ) // game loop
 *		{
 *			// do some routines
 *			// ...
 *			snd.PlaySDLSound( snd1 );
 *			// ...
 *			snd.PausePlaying(); // stops playing on current position
 *			// ...
 *			snd.StartPlaying(); // continues playing on current position
 *			// ...
 *			snd.Loop(snd2, false); // you always can change the loop to false
 *			// ...
 *			snd.StopAllSounds(); // stops all sounds and sets position to 0
 *		}
 *
 *		return 0;
 *	}
 */
