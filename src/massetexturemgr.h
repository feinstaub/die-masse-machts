#ifndef MASSETEXTUREMGR_H
#define MASSETEXTUREMGR_H

#include "SDL.h"
#include <vector>

class MasseTextureMgr
{
public:
    MasseTextureMgr();

    /**
     * @brief RegisterTexture for later deletion
     * @param texture
     * @return the given texture for command chaining
     */
    SDL_Texture* RegisterTexture(SDL_Texture* texture);

    /**
     * @brief Destroys all registered textures
     */
    void DestroyAll();

private:
    std::vector<SDL_Texture*> _textureList;
};

#endif // MASSETEXTUREMGR_H
